<?php

/**
 * Hook implementation for Two Kings installation profile for Drupal 8.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;

/**
 * @file
 * Enables modules and site configuration for the Two Kings profile.
 */

/**
 * Implements hook_form_install_configure_form_alter().
 */
function twokings_form_install_configure_form_alter(&$form, FormStateInterface $form_state) {

  // Site info defaults
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];

  // Account defaults.
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['admin_account']['account']['mail']['#default_value'] = 'support@twokings.nl';

  // Date/time defaults.
  $form['regional_settings']['site_default_country']['#default_value'] = 'NL';
  $form['regional_settings']['date_default_timezone']['#default_value'] = 'Europe/Amsterdam';
}

/**
 * Implements hook_install_tasks().
 */
function twokings_install_tasks(&$install_state) {
  $tasks = [
    'twokings_create_content' => [
      'display_name' => t('Create content'),
      'type' => 'normal',
    ],
  ];
  return $tasks;
}

/**
 * Installation task callback: Create some default content.
 *
 * @see twokings_install_tasks
 */
function twokings_create_content() {

  // Create nodes for 403 and 404 pages.
  $values = [
    'title' => 'No access',
    'type' => 'system_page',
    'uid' => 1,
    'status' => 1,
  ];
  $node = Node::create($values);
  $node->save();
  \Drupal::service('config.factory')
    ->getEditable('system.site')
    ->set('page.403', '/node/' . $node->id())
    ->save();

  $values['title'] = 'Page not found';
  $node = Node::create($values);
  $node->save();
  \Drupal::service('config.factory')
    ->getEditable('system.site')
    ->set('page.404', '/node/' . $node->id())
    ->save();
}
