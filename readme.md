# Two Kings Installation profile for Drupal 8 websites.

## Usage
Place this profile inside the /profiles directory of a Drupal site and start an
installation as usual. Note that during the installation, there is no option to 
select this profile, it behaves like a distribution.

During installation, the site is configured and some basic content is created.
 
## Version update
Regularly update the Drupal module version numbers in the composer.json files 
with the latest stable release.
 
## Modify the profile configuration
In case you want to modify this installation profile, i.e. change how new sites
are configured that are created with this profile, use the following steps:

- Create an new installation with this profile.
- Export the configuration before making changes to a directory outside of this
  installation profile.
- Commit the exported configuration as a reference.
- Make configuration modifications.
- Export the modified configuration.
- Copy the changed files to the twokings/config directory.
- From these new or modified files, 
  - Remove the first line, that start with "uuid: ..."
  - Remove the line that starts with "_core: ..." and the (indented) line that follows.
- Commit the changed configuration files in the twokings repository
- Check the configuration by making a new installation with the modified profile.

IMPORTANT Do *not* add the core.extensions configuration file to twokings/config.

When new modules are added to the profile, add them to the composer.json file
including the desired version.
